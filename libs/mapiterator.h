#pragma once

// IMPORT STANDARD LIBRARIES
#include <string>
#include <map>


class MapIterator {
    private:
        std::map<std::pair<int, int>, std::string> data;
        unsigned int rows;
        unsigned int columns;

    public:
        MapIterator(unsigned int rows, unsigned int columns);
        // TODO: How do you delete a map?
        /* ~MapIterator() { */
        /*     delete this->data; */
        /* } */

    std::string& operator[](std::pair<int, int> coordinate);
};
