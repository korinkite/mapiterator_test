// IMPORT LOCAL LIBRARIES
#include "mapiterator.h"

MapIterator::MapIterator(unsigned int rows, unsigned int columns) {
    this->rows = rows;
    this->columns = columns;
}

std::string& MapIterator::operator[](std::pair<int, int> coordinate) {
    return this->data[coordinate];
}
