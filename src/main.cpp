// IMPORT STANDARD LIBRARIES
#include <iostream>

// IMPORT LOCAL LIBRARIES
#include "mapiterator.h"

int main() {
    auto map = MapIterator(2, 10);

    map[{1, 3}] = "thing";
    map[{2, 3}] = "another";
    map[{2, 4}] = "still";
    map[{2, 5}] = "here";
    map[{2, 7}] = "you know";

    std::cout << "Test" << std::endl;
}
